// google map
var map = '';
var center;

function initialize() {
    var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(40.7679619,-73.9800172),
        scrollwheel: false
    };

    map = new google.maps.Map(document.getElementById('map-canvas'),  mapOptions);

    google.maps.event.addDomListener(map, 'idle', function() {
        calculateCenter();
    });

    google.maps.event.addDomListener(window, 'resize', function() {
        map.setCenter(center);
    });
}

function calculateCenter() {
    center = map.getCenter();
}

function loadGoogleMap(){
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' + 'callback=initialize';
    document.body.appendChild(script);
}



$(window).on('load', function(e) {
console.log('load');
    /* FlexSlider */
    $('.flexslider').flexslider({
        animation: "fade",
        directionNav: false
    });

    new WOW().init();

    loadGoogleMap();

    var lien_css = document.createElement('link');
    lien_css.href = "css/combinate.css";
    lien_css.rel = "stylesheet";
    lien_css.type = "text/css";
    document.getElementsByTagName("head")[0].appendChild(lien_css);


});


// Hide mobile menu after clicking on a link
$('.navbar-collapse a').click(function(){
    $(".navbar-collapse").collapse('hide');
});

//jQuery for page scrolling feature - requires jQuery Easing plugin

$('.navbar-default a, a').bind('click', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top - 68
    }, 1000);
    event.preventDefault();
});

// isotope

$(document).ready(function(e){

    if ( $('.iso-box-wrapper').length > 0 ) {

        var $container  = $('.iso-box-wrapper'),
            $imgs     = $('.iso-box img');

        $container.imagesLoaded(function () {

            $container.isotope({
                layoutMode: 'fitRows',
                itemSelector: '.iso-box'
            });

            $imgs.load(function(){
                $container.isotope('reLayout');
            })

        });

        //filter items on button click
        $('.filter-wrapper li a').click(function(){

            var $this = $(this), filterValue = $this.attr('data-filter');

            $container.isotope({
                filter: filterValue,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false,
                }
            });

            // don't proceed if already selected
            if ( $this.hasClass('selected') ) {
                return false;
            }

            var filter_wrapper = $this.closest('.filter-wrapper');
            filter_wrapper.find('.selected').removeClass('selected');
            $this.addClass('selected');

            return false;
        });

    }

});




   