module.exports = function(grunt) {

	grunt.initConfig({
		  concat_css: {
		    options: {},
		    files: {
		      'minify/compiled.css': ['css/*.css'],
		    },
		  },
		});
		
		grunt.loadNpmTasks('grunt-concat-css');

};